package com.gaurav.appdev.ticket.ticket.events;

public record Organizer(int id,
                        String name,
                        String description) {
}
