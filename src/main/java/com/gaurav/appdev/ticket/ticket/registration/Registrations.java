package com.gaurav.appdev.ticket.ticket.registration;

import jakarta.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Registrations")
public record Registrations(
        @Id String id, //@Id always generate new unique string ID
                            @NotNull(message = "Product Id Required") Integer productId,
                            String ticketCode,
                            @NotNull String attendeeName) {
}
