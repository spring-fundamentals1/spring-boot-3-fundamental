package com.gaurav.appdev.ticket.ticket.JPARepositories;

import com.gaurav.appdev.ticket.ticket.JPAEvents.Organizer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizerRepositoryJPA extends JpaRepository<Organizer, Integer> {
    // findAll() is already defined hence we did not required to mentioned
}
