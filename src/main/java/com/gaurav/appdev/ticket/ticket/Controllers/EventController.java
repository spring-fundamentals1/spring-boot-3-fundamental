package com.gaurav.appdev.ticket.ticket.Controllers;

import com.gaurav.appdev.ticket.ticket.events.Event;
import com.gaurav.appdev.ticket.ticket.events.Organizer;
import com.gaurav.appdev.ticket.ticket.events.Product;
import com.gaurav.appdev.ticket.ticket.repositories.EventRepository;
import com.gaurav.appdev.ticket.ticket.repositories.OrganizerRepository;
import com.gaurav.appdev.ticket.ticket.repositories.ProductRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController   //so spring bean and spring mav will know it's an controller class
public class EventController {


    private final OrganizerRepository organizerRepository;
    private final EventRepository eventRepository;
    private final ProductRepository productRepository;


    public EventController(OrganizerRepository organizerRepository,
                           EventRepository eventRepository,
                           ProductRepository productRepository) {
        this.organizerRepository = organizerRepository;
        this.eventRepository = eventRepository;
        this.productRepository = productRepository;
    }

    //@GetMapping(path = "/organizer")
    @RequestMapping(method = RequestMethod.GET, path = "/organizer")
    public List<Organizer> getOrganizer() {
        return organizerRepository.findAll();
    }


    @GetMapping(path = "/events")
    public List<Event> getEventsByOrganizer(@RequestParam("organizerId") int organizerId) {
        return eventRepository.findByOrganizerId(organizerId);
    }


    @GetMapping(path = "/events/{id}")
    public Event getEventByID(@PathVariable("id") int eventId) {
        return eventRepository.findById(eventId)
                .orElseThrow(()-> new NoSuchElementException("Event with id " + eventId + " not found"));
    }


    @GetMapping(path = "/products")
    //@RequestMapping(method = RequestMethod.GET, path = "/products")
    public List<Product> getProductsByEvent(@RequestParam("eventId") int eventId) {
        return productRepository.findByEventId(eventId);
    }


}
