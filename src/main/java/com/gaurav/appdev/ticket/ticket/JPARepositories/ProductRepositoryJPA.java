package com.gaurav.appdev.ticket.ticket.JPARepositories;

import com.gaurav.appdev.ticket.ticket.JPAEvents.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepositoryJPA extends JpaRepository<Product, Integer> {

    List<Product> findByEventId(int eventId);
}
