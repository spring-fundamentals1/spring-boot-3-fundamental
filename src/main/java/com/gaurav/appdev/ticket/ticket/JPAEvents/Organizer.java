package com.gaurav.appdev.ticket.ticket.JPAEvents;

import jakarta.persistence.*;

@Entity //this @Entity required class not record hence we created this new class.
@Table(name="organizers")
public class Organizer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //this tells hibernet the value is automatically generated.
     private int id;

    @Column(nullable = false)
     private String name;

    @Column
     private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
