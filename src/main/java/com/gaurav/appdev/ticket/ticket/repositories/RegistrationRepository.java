package com.gaurav.appdev.ticket.ticket.repositories;

import com.gaurav.appdev.ticket.ticket.registration.Registrations;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
public interface RegistrationRepository extends MongoRepository<Registrations, String> {

    Optional<Registrations> findByTicketCode(String ticketCode);

    void deleteByTicketCode(String ticketCode);




    //Commenting the below code as we have not required it after extending MongoRepositiory

    /*
    private static final AtomicInteger ID_GENERATOR = new AtomicInteger();


    private final Map<String, Registrations> registrationByTicketCode = new ConcurrentHashMap<>();

    public Registrations create(Registrations registrations){
        int id = ID_GENERATOR.incrementAndGet();
        String ticketCode = UUID.randomUUID().toString();
        Registrations newRegistrations = new Registrations(id, registrations.productId(), ticketCode,
                registrations.attendeeName());
        registrationByTicketCode.put(ticketCode, newRegistrations);
        return newRegistrations;
    }

    public Optional<Registrations> findByTicketCode(String ticketCode) {
        return Optional.ofNullable(registrationByTicketCode.get(ticketCode));
    }

    public Registrations update(Registrations registrations) {
        String ticketCode = registrations.ticketCode();

        Optional<Registrations> optionalRegistrations = findByTicketCode(ticketCode);
        if(optionalRegistrations.isPresent()){
            Registrations existingRegistrations = optionalRegistrations.get();
            Registrations newRegistrations = new Registrations(existingRegistrations.id(),
                    existingRegistrations.productId(),
                    existingRegistrations.ticketCode(),
                    registrations.attendeeName());
            registrationByTicketCode.put(ticketCode, newRegistrations);
            return newRegistrations;
        }
        else {
            throw new NoSuchElementException("Registration with ticket code " + ticketCode + " not found.");
        }
    }

    public void deleteByTicketCode(String ticketCode) {
        registrationByTicketCode.remove(ticketCode);
    }

    */

}
