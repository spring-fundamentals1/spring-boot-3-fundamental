package com.gaurav.appdev.ticket.ticket.JPAControllers;



import com.gaurav.appdev.ticket.ticket.JPAEvents.Event;
import com.gaurav.appdev.ticket.ticket.JPAEvents.Organizer;
import com.gaurav.appdev.ticket.ticket.JPAEvents.Product;
import com.gaurav.appdev.ticket.ticket.JPARepositories.EventRepositoryJPA;
import com.gaurav.appdev.ticket.ticket.JPARepositories.OrganizerRepositoryJPA;
import com.gaurav.appdev.ticket.ticket.JPARepositories.ProductRepositoryJPA;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController   //so spring bean and spring mav will know it's an controller class
@RequestMapping(path="/JPA")
public class JPAEventController {


    private final OrganizerRepositoryJPA organizerRepository;
    private final EventRepositoryJPA eventRepository;
    private final ProductRepositoryJPA productRepository;


    public JPAEventController(OrganizerRepositoryJPA organizerRepository,
                           EventRepositoryJPA eventRepository,
                           ProductRepositoryJPA productRepository) {
        this.organizerRepository = organizerRepository;
        this.eventRepository = eventRepository;
        this.productRepository = productRepository;
    }

    //@GetMapping(path = "/organizer")
    @RequestMapping(method = RequestMethod.GET, path = "/organizer")
    public List<Organizer> getOrganizer() {
        return organizerRepository.findAll();
    }


    @GetMapping(path = "/events")
    public List<Event> getEventsByOrganizer(@RequestParam("organizerId") int organizerId) {
        return eventRepository.findByOrganizerId(organizerId);
    }


    @GetMapping(path = "/events/{id}")
    public Event getEventByID(@PathVariable("id") int eventId) {
        return eventRepository.findById(eventId)
                .orElseThrow(()-> new NoSuchElementException("Event with id " + eventId + " not found"));
    }


    @GetMapping(path = "/products")
    //@RequestMapping(method = RequestMethod.GET, path = "/products")
    public List<Product> getProductsByEvent(@RequestParam("eventId") int eventId) {
        return productRepository.findByEventId(eventId);
    }


}
