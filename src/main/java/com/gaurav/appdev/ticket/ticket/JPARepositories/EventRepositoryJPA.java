package com.gaurav.appdev.ticket.ticket.JPARepositories;

import com.gaurav.appdev.ticket.ticket.JPAEvents.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EventRepositoryJPA extends JpaRepository<Event, Integer> {

    List<Event> findByOrganizerId(int organizerId);
}
