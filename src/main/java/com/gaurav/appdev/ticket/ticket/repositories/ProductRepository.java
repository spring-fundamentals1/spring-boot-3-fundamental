package com.gaurav.appdev.ticket.ticket.repositories;

import com.gaurav.appdev.ticket.ticket.events.Product;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public class ProductRepository {

    private final List<Product> productList = List.of(
            new Product(801,201, "Regular", "Regular Confereance Ticket", new BigDecimal("199.00")),
            new Product(802,201, "Standard", "Standard Confereance Ticket", new BigDecimal("499.00")),
            new Product(803,203, "Regular", "Regular Confereance Ticket", new BigDecimal("199.00")),
            new Product(804,202, "Standard", "Standard Confereance Ticket", new BigDecimal("499.00")),
            new Product(805,202, "Premium", "Premium Confereance Ticket", new BigDecimal("899.00")),
            new Product(806,203, "Premium", "Premium Confereance Ticket", new BigDecimal("899.00"))
    ) ;

    public List<Product> findByEventId(int eventId) {
        return productList.stream().filter(product -> product.eventId() == eventId).toList();
    }
}
