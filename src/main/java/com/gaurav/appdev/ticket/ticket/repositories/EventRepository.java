package com.gaurav.appdev.ticket.ticket.repositories;

import com.gaurav.appdev.ticket.ticket.events.Event;
import com.gaurav.appdev.ticket.ticket.events.Organizer;
import com.gaurav.appdev.ticket.ticket.events.Venue;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public class EventRepository {

    private final List<Event> events = List.of(
            new Event(201, "Goldman Confereance",
                    new Organizer(103, "Goldman", "Goldman Corporation"),
                    new Venue(103,"Goldman Main Office", "City Road", "Banglore", "IN"),
                    LocalDate.of(2024,06,29),
                    LocalDate.of(2024,06,30)
            ),
            new Event(202, "Cotivity Confereance",
                    new Organizer(105, "Cotivity", "Cotivity Corporation"),
                    new Venue(103,"Cotivity Main Office", "Cotivity Road", "Pune", "IN"),
                    LocalDate.of(2024,06,20),
                    LocalDate.of(2024,06,24)
            ),
            new Event(203, "Tomtom Confereance",
                    new Organizer(104, "Tomtom", "Tomtom Corporation"),
                    new Venue(103,"Tomtom Main Office", "Tomtom campus", "Mumbai", "IN"),
                    LocalDate.of(2024,06,19),
                    LocalDate.of(2024,06,24)
            )
    ) ;

    public List<Event> findByOrganizerId(int organizerId) {
        return events.stream().filter(event -> event.organizer().id() == organizerId ).toList();
    }

    public Optional<Event> findById(int id) {
        return events.stream().filter(event -> event.id() == id).findAny();
    }
}
