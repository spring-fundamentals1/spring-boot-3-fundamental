package com.gaurav.appdev.ticket.ticket.events;

public record Venue(int id,
        String nanem,
        String street,
        String city,
        String country) {
}
