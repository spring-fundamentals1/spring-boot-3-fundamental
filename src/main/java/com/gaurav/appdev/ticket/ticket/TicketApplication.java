package com.gaurav.appdev.ticket.ticket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketApplication.class, args);
	}



	// SW Architecture & Practices
	//https://www.youtube.com/watch?v=mRHSCQ9PYso&list=PLLAi7xIKHkis7_jgvLLpXavwprEOqKVJQ

	// Multithreading Complete Course
	// https://www.youtube.com/watch?v=jkdyrF2cNpw&list=PLGdlDsaVhUDGwjkW9_KWFDmqV-M0tTS2I





	//Some Useful Docker and Mongo Commands

	/*
	docker compose up -d    -> -d for deattach which means it's run in background
	docker exec -it <container-name> mongosh   -> to enter into db shell
	-> show collections
	-> db.<collection-name>.find()  -> to see all entries in collection.


	* */
}
