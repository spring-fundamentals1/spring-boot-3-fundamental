package com.gaurav.appdev.ticket.ticket.Controllers;

import com.gaurav.appdev.ticket.ticket.registration.Registrations;
import com.gaurav.appdev.ticket.ticket.repositories.RegistrationRepository;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.UUID;

@RestController
@RequestMapping(path="/registrations")
public class RegistrationController {

    private final RegistrationRepository registrationRepository;

    public RegistrationController(RegistrationRepository registrationRepository){
        this.registrationRepository = registrationRepository;
    }

    @PostMapping
    public Registrations create(@RequestBody @Valid Registrations registrations){
        if(registrations.ticketCode() == null) {
            String ticketCode = UUID.randomUUID().toString();
            return registrationRepository.save(new Registrations(null, registrations.productId(),
                    ticketCode, registrations.attendeeName()));
        }
        return registrationRepository.save(registrations);
    }

    @GetMapping(path="/{ticketCode}")
    public Registrations get(@PathVariable("ticketCode") String ticketId) {
        return registrationRepository.findByTicketCode(ticketId)
                .orElseThrow(()-> new NoSuchElementException("Registration with ticker code " + ticketId + " not found"));
    }

    @PatchMapping
    public Registrations update(@RequestBody Registrations registrations) {
        Registrations oldRegisration = registrationRepository.findByTicketCode(registrations.ticketCode())
                .orElseThrow(() ->
                        new NoSuchElementException("Registration with ticker code "
                                + registrations.ticketCode() + " not found"));

        return registrationRepository.save(new Registrations(oldRegisration.id(),
                registrations.productId(),registrations.ticketCode(), registrations.attendeeName()));
    }

    @DeleteMapping(path = "/{ticketCode}")
    public String delete(@PathVariable String ticketCode) {
        registrationRepository.deleteByTicketCode(ticketCode);
        return "Successfully deleted ticket " + ticketCode;
    }



}
