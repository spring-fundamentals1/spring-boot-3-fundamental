package com.gaurav.appdev.ticket.ticket.repositories;

import com.gaurav.appdev.ticket.ticket.events.Venue;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class VenueRepository {

    private final List<Venue> veneues = List.of(
            new Venue(101, "Binarious Pune Office", "Shastrinagar", "Pune", "IN"),
            new Venue(102, "Commreszone Office", "Yerwada", "Pune", "IN")
    );

    public Optional<Venue> findById(int id) {
        return veneues.stream().filter(venue -> venue.id() == id).findAny();
        //this return either empty or contain any matching value.
    }
}
