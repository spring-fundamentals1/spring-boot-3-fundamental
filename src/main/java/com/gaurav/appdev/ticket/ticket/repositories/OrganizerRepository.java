package com.gaurav.appdev.ticket.ticket.repositories;


import com.gaurav.appdev.ticket.ticket.events.Organizer;
import com.gaurav.appdev.ticket.ticket.events.Venue;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class OrganizerRepository {

    private final List<Organizer> organizers = List.of(
            new Organizer(101, "TIBCO", "TIBCO/ Cloud Software Group"),
            new Organizer(102, "ABS", "Anti-lock braking system")
    );

    public List<Organizer> findAll() {
        return organizers;
    }

}
